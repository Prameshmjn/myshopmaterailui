import axios from "axios";

const http = axios.create({
  baseURL: "http://localhost:8080/api",
  responseType: "json",
  timeout: 5000,
  timeoutErrorMessage: "server took too long to respond",
});
const getHeaders = (secured) => {
  let option = {
    "Content-type": "application/json",
  };
  if (secured) {
    option["Authorization"] = localStorage.getItem("token");
  }
  return option;
};

export const GET = (url, isSecure = false, params = {}) => {
  return http.get(url, {
    headers: getHeaders(isSecure),
    params,
  });
};

export const POST = (url, data, isSecure = false, params = {}) => {
  return http.post(url, data, {
    headers: getHeaders(isSecure),
    params,
  });
};

export const UPDATE = (url, data, isSecure = false, params = {}) => {
  return http.put(url, data, {
    headers: getHeaders(isSecure),
    params,
  });
};

export const DELETE = (url, isSecure = false, params = {}) => {
  return http.delete(url, {
    headers: getHeaders(isSecure),
    params,
  });
};

export const UPLOAD = (
  method,
  url,
  data,
  fileData = [],
  filesToRemove = []
) => {
  return new Promise((resolve, reject) => {
    debugger;
    const xhr = new XMLHttpRequest();
    const formData = new FormData();
    console.log("in http request");

    if (fileData.length > 0) {
      fileData.forEach((file) => formData.append("image", file, file.name));
    }
    if (filesToRemove.length) {
      formData.append("filesToRemove", filesToRemove);
    }

    for (const key in data) {
      formData.append(key, data[key]);
    }
    xhr.onreadystatechange = () => {
      console.log("ready state is", xhr.readyState);
      console.log("status is", xhr.status);
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          resolve(xhr.response);
        } else {
          reject(xhr.response);
        }
      }
    };
    xhr.open(
      method,
      `http://localhost:8080/api${url}?token=${localStorage.getItem("token")}`,
      true
    );
    xhr.send(formData);
  });
};
