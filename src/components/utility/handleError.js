import { toast } from "react-toastify";

export const handleError = (err) => {
  let error = err.response;
  let errMsg = "something went wrong";
  if (err) {
    errMsg = error && error.data && error.data.msg;
  }
  toast.error(errMsg);
};
