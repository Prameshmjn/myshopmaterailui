import moment from "moment";

export const handleDate = (date, format = "YYYY-MM-DD") => {
  if (date) {
    return moment(date).format(format);
  }
};
