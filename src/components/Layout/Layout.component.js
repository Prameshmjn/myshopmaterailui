import { makeStyles } from "@material-ui/core";
import { Navbar } from "../Common/Navbar/Navbar.component";
import { Sidebars } from "../Common/Sidebar/Sidebar.component";
import Headered from "./component/Appbar.js";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      display: "flex",
    },
  };
});

export const Layout = ({ children }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div>
        <Headered />
      </div>
      <div>
        <Sidebars />
      </div>
      <div>
        <h1>i love you</h1>
      </div>
    </div>
  );
};
