import { Button, makeStyles, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { handleError } from "../../utility/handleError";
import { POST } from "../../utility/httpClient";
import { showSuccess } from "../../utility/showMessage";

const useStyles = makeStyles((theme) => {
  return {
    flex: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      margin: theme.spacing(2, 0),
    },
    form_container: {
      width: "85%",
      height: "80vh",
      margin: "0 auto",

      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    textFileds: {
      margin: "15px 0px",
    },
    notchedOutline: {
      borderWidth: "1px",
      borderColor: "hotPink !important",
    },
    form_wrapper: {
      height: "40vh",
    },
    show_info: {
      display: "flex",
      justifyContent: "space-between",
    },

    [theme.breakpoints.down("md")]: {
      forgot: {
        fontSize: "15px",
      },
      textFileds: {
        margin: "7px 0px",
      },
    },
  };
});

export const Login = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernametError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      history.push("/dashboard");
    }
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setUsernametError(false);
    setPasswordError(false);
    if (!username) {
      setUsernametError(true);
    }
    if (!password) {
      setPasswordError(true);
    }
    if (username && password) {
      const newUsers = { username, password };
      setUsers([...users, newUsers]);
      setIsSubmitting(true);
      POST("/auth/login", newUsers)
        .then((response) => {
          localStorage.setItem("token", response.data.token);
          localStorage.setItem("user", JSON.stringify(response.data.user));
          showSuccess(`welcome ${response.data.user.username}`);
          history.push("/dashboard");
        })
        .catch((err) => handleError(err));
      setIsSubmitting(false);
    }
  };

  return (
    <div className={classes.form_container}>
      <div className={classes.form_wrapper}>
        <form onSubmit={handleSubmit} className={classes.formwrapper}>
          <Typography variant="h5" color="secondary">
            Login
          </Typography>
          <TextField
            label="Username"
            variant="outlined"
            onChange={(e) => setUsername(e.target.value)}
            className={classes.textFileds}
            color="secondary"
            fullWidth
            error={usernameError}
            InputProps={{
              classes: {
                notchedOutline: classes.notchedOutline,
              },
              style: { color: "hotPink" },
            }}
            InputLabelProps={{
              style: { color: "#ab47bc" },
            }}
          ></TextField>
          <TextField
            label="Password"
            variant="outlined"
            onChange={(e) => setPassword(e.target.value)}
            className={classes.textFileds}
            color="secondary"
            fullWidth
            error={passwordError}
            InputProps={{
              classes: {
                notchedOutline: classes.notchedOutline,
              },
              style: { color: "hotPink" },
            }}
            InputLabelProps={{
              style: { color: "#ab47bc" },
            }}
          ></TextField>
          {isSubmitting ? (
            <Button variant="outlined" color="secondary">
              Logining in...
            </Button>
          ) : (
            <Button variant="outlined" color="secondary" type="submit">
              Login
            </Button>
          )}
        </form>
        <div className={classes.flex}>
          <Typography variant="body1" color="secondary">
            Don't have an account?
          </Typography>
          <Button color="secondary" variant="outlined">
            SignUP
          </Button>
        </div>
        <div className={classes.flex}>
          <Typography
            variant="body1"
            color="secondary"
            className={classes.forgot}
          >
            Forgot your password?
          </Typography>
          <Typography
            variant="body1"
            color="secondary"
            className={classes.forgot}
            onClick={() => console.log("clicked")}
          >
            Reset
          </Typography>
        </div>
      </div>
    </div>
  );
};
