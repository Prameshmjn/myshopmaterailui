import { useState } from "react";
import { useHistory } from "react-router";
import { UPLOAD } from "../../utility/httpClient";
import { showInfo } from "../../utility/showMessage";
import { handleError } from "../../utility/handleError";
import { ProductForm } from "../ProductForm/ProductForm.component";

export const AddProduct = (props) => {
  const history = useHistory();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const addProduct = (data, image) => {
    setIsSubmitting(true);
    // POST("/product", data, true)
    //   .then((response) => {
    //     console.log(response.data);
    //     history.push("/viewproduct");
    //   })
    //   .catch((err) => console.log(err));
    UPLOAD("POST", "/product", data, image)
      .then((response) => {
        showInfo("Product added sucessfully");
        history.push("/viewproduct");
      })
      .catch((err) => handleError(err));

    setIsSubmitting(false);
  };
  return (
    <div>
      <ProductForm
        submitCallBack={addProduct}
        isEditMode={false}
        isSubmitting={isSubmitting}
      />
    </div>
  );
};
