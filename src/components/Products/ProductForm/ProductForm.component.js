import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  InputLabel,
  makeStyles,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import React from "react";
import { useEffect, useState } from "react";
import { handleDate } from "../../utility/handleDate";
import "./ProductForm.component.css";

const useStyle = makeStyles((theme) => ({
  formFilled: {
    margin: theme.spacing(2),
  },
  notchedOutline: {
    borderWidth: "1px",
    borderColor: "hotPink !important",
  },
  choice: {
    margin: theme.spacing(1.5, 0),
  },
  imageAlign: {
    display: "flex",
    flexDirection: "column",
  },
  imageAlign1: {
    display: "flex",
    flexDirection: "row",
    margin: theme.spacing(1, 0),
  },
}));

export const ProductForm = (props) => {
  const classes = useStyle();
  const [formInfo, setFormInfo] = useState({
    name: "",
    category: "",
    description: "",
    color: "",
    brand: "",
    price: "",
    quantity: "",
    status: "",
    tags: "",
    discountType: "",
    discountValue: "",
    warrentyPeriod: "",
    salesDate: `${handleDate(Date.now())}`,
    purchasedDate: `${handleDate(Date.now())}`,
    manuDate: `${handleDate(Date.now())}`,
    expiryDate: `${handleDate(Date.now())}`,
    discountedItem: false,
    warrentyStatus: false,
    isReturnEligible: false,
  });
  const [image, setImage] = useState([]);
  let [previousImages, setPreviousImages] = useState([]);
  let [filesToRemove, setFilesToRemove] = useState([]);
  let { isEditMode, productData } = props;
  useEffect(() => {
    console.log("product data is", productData);
    if (productData) {
      setFormInfo({
        ...formInfo,
        ...productData,
        discountedItem:
          productData.discount && productData.discount.discountedItem
            ? true
            : false,
        discountType:
          productData.discount && productData.discount.discountedItem
            ? productData.discount["discountType"]
            : "",
        discountValue:
          productData.discount && productData.discount.discountedItem
            ? productData.discount["discountValue"]
            : "",
        salesDate: productData.salesDate
          ? handleDate(productData.salesDate)
          : "",
        purchasedDate: productData.purchasedDate
          ? handleDate(productData.purchasedDate)
          : "",
        manuDate: productData.manuDate ? handleDate(productData.manuDate) : "",
        expiryDate: productData.expiryDate
          ? handleDate(productData.salesDate)
          : "",
        warrentyPeriod: productData.warrentyStatus
          ? productData.warrentyPeriod
          : "",
      });
      previousImages = (productData.image || []).map(
        (item, index) => `http://localhost:8080/files/images/${item}`
      );
      setPreviousImages([...previousImages]);
    }
  }, [productData]);

  const handleChange = (e) => {
    const { checked, name, value, type, files } = e.target;
    if (type === "checkbox") {
      setFormInfo({ ...formInfo, [name]: checked });
      return;
    }
    if (type === "file") {
      image.push(files[0]);
      setImage(image);
    }

    setFormInfo({ ...formInfo, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.submitCallBack(formInfo, image, filesToRemove);
  };
  const removeSelectedImage = (index) => {
    image.splice(index, 1);
    setImage([...image]);
  };
  const removePreviousSelectedImages = (item, index) => {
    previousImages.splice(index, 1);
    setPreviousImages([...previousImages]);
    filesToRemove.push(item);
    setFilesToRemove(filesToRemove);
  };
  console.log("in image remove", filesToRemove);
  console.log("image is", image);

  return (
    <div>
      <Typography variant="h4" color="secondary" gutterBottom>
        {isEditMode ? "Update Product" : "Add Product"}
      </Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          variant="outlined"
          color="secondary"
          label="Name"
          value={formInfo.name}
          name="name"
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Category"
          name="category"
          value={formInfo.category}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Description"
          name="description"
          value={formInfo.description}
          onChange={(e) => handleChange(e)}
          multiline
          rows={5}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Color"
          name="color"
          value={formInfo.color}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Price"
          name="price"
          value={formInfo.price}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Brand"
          name="brand"
          value={formInfo.brand}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Quantity"
          name="quantity"
          value={formInfo.quantity}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          color="secondary"
          label="Tags"
          name="tags"
          value={formInfo.tags}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          type="date"
          color="secondary"
          label="Manufactured Date"
          name="manuDate"
          value={formInfo.manuDate}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          type="date"
          color="secondary"
          label="Expiry Date"
          name="expiryDate"
          value={formInfo.expiryDate}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          type="date"
          color="secondary"
          label="Purchased Date"
          name="purchasedDate"
          value={formInfo.purchasedDate}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>
        <TextField
          variant="outlined"
          type="date"
          color="secondary"
          label="Sales Date"
          name="salesDate"
          value={formInfo.salesDate}
          onChange={(e) => handleChange(e)}
          fullWidth
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
          className={classes.formFilled}
        ></TextField>

        <FormControl className={classes.formFilled}>
          <InputLabel htmlFor="discountType">Status</InputLabel>
          <Select
            native
            onChange={handleChange}
            value={formInfo.status}
            name="status"
          >
            <option value="">(Select Status)</option>
            <option value="available">Available</option>
            <option value="booked">Booked</option>
            <option value="out of stock">Out Of Stock</option>
          </Select>
        </FormControl>

        <FormGroup className={classes.formFilled}>
          <FormControlLabel
            control={
              <Checkbox
                checked={formInfo.discountedItem}
                onChange={handleChange}
                name="discountedItem"
                value={formInfo.discountedItem}
              />
            }
            label="Discounted Item"
          />

          {formInfo.discountedItem ? (
            <div>
              <FormControl className={classes.choice}>
                <InputLabel htmlFor="discountType">Dicount Type</InputLabel>
                <Select
                  native
                  onChange={handleChange}
                  value={formInfo.discountType}
                  name="discountType"
                >
                  <option value="">(Select Discount Type)</option>
                  <option value="percentage">Percentage</option>
                  <option value="quantity">Quantity</option>
                  <option value="value">Value</option>
                </Select>
              </FormControl>
              <TextField
                variant="outlined"
                color="secondary"
                label="Discount Value"
                name="discountValue"
                value={formInfo.discountValue}
                onChange={handleChange}
                fullWidth
                className={classes.choice}
                InputProps={{
                  classes: {
                    notchedOutline: classes.notchedOutline,
                  },
                  style: { color: "hotPink" },
                }}
                InputLabelProps={{
                  style: { color: "#ab47bc" },
                }}
              ></TextField>
            </div>
          ) : null}

          <FormControlLabel
            control={
              <Checkbox
                checked={formInfo.warrentyStatus}
                onChange={handleChange}
                name="warrentyStatus"
                value={formInfo.warrentyStatus}
              />
            }
            label="Warrenty Status"
          />
          {formInfo.warrentyStatus ? (
            <TextField
              variant="outlined"
              color="secondary"
              label="Warrenty Period"
              name="warrentyPeriod"
              value={formInfo.warrentyPeriod}
              onChange={handleChange}
              fullWidth
              className={classes.choice}
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
                style: { color: "hotPink" },
              }}
              InputLabelProps={{
                style: { color: "#ab47bc" },
              }}
            ></TextField>
          ) : null}

          <FormControlLabel
            control={
              <Checkbox
                checked={formInfo.isReturnEligible}
                onChange={handleChange}
                name="isReturnEligible"
                value={formInfo.isReturnEligible}
              />
            }
            label="Return Eligible"
          />
        </FormGroup>

        <TextField
          variant="outlined"
          color="secondary"
          label="Choose Image"
          type="file"
          onChange={handleChange}
          fullWidth
          className={classes.choice}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
            style: { color: "hotPink" },
          }}
          InputLabelProps={{
            style: { color: "#ab47bc" },
          }}
        ></TextField>
        <div className={classes.imageAlign}>
          {(image || []).map((img, index) => (
            <div key={index} className={classes.imageAlign1}>
              <img
                src={URL.createObjectURL(img)}
                width="200px"
                alt="alt.png"
              ></img>
              <IconButton onClick={() => removeSelectedImage(index)}>
                <Delete style={{ color: "red" }} />
              </IconButton>
            </div>
          ))}
        </div>
        <div className={classes.imageAlign}>
          {(previousImages || []).map((img, index) => (
            <div key={index} className={classes.imageAlign1}>
              <img src={img} width="200px" alt="alt.png"></img>
              <IconButton
                onClick={() => removePreviousSelectedImages(img, index)}
              >
                <Delete style={{ color: "red" }} />
              </IconButton>
            </div>
          ))}
        </div>

        {props.isSubmitting ? (
          <Button variant="outlined" color="secondary" type="submit">
            Submitting ...
          </Button>
        ) : (
          <Button variant="outlined" color="secondary" type="submit">
            Submit
          </Button>
        )}
      </form>
    </div>
  );
};
