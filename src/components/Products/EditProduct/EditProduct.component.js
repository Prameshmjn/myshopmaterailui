import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import { GET, UPDATE, UPLOAD } from "../../utility/httpClient";
import { ProductForm } from "../ProductForm/ProductForm.component";

export const EditProduct = (props) => {
  const location = useLocation();
  const history = useHistory();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [productData, setProductData] = useState([]);
  const productId = location.pathname.split("/")[2];
  useEffect(() => {
    GET(`/product/${productId}`, true)
      .then((response) => {
        setProductData(response.data);
      })
      .catch((err) => console.log(err));
  }, [productId]);
  const handleEdit = (data, image, filesToRemove) => {
    console.log("data is", data);
    setIsSubmitting(true);
    UPLOAD("PUT", `/product/${productId}`, data, image, filesToRemove)
      .then((response) => {
        history.push("/viewproduct");
      })
      .catch((err) => console.log(err))
      .finally(() => {
        setIsSubmitting(false);
      });
  };
  return (
    <div>
      <ProductForm
        isSubmitting={isSubmitting}
        isEditMode={true}
        productData={productData}
        submitCallBack={handleEdit}
      ></ProductForm>
    </div>
  );
};
