import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { Delete, Edit } from "@material-ui/icons";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 300,
    position: "relative",
  },
  media: {
    height: 280,
    width: "100%",
    objectFit: "cover",
  },
  content: {
    background: "rgba(0,0,0,0.1)",
  },
  btnAction: {
    position: "absolute",
    zIndex: 3,
    top: 0,
    right: 0,
  },
  edit: {
    color: "blue",
  },
  delete: {
    color: "red",
  },
}));

export const ProductCard = ({ product, deleteProduct, isProtected }) => {
  const classes = useStyles();
  const history = useHistory();
  const handleEdit = (id) => {
    history.push(`/editproduct/${id}`);
  };
  const handleDelete = (productId) => {
    deleteProduct(productId);
  };

  return (
    <div>
      <Card className={classes.root}>
        {isProtected ? (
          <CardActions className={classes.btnAction}>
            <IconButton
              aria-label="edit product"
              onClick={() => handleEdit(product._id)}
            >
              <Edit className={classes.edit} />
            </IconButton>
            <IconButton
              aria-label="delete product"
              onClick={() => handleDelete(product._id)}
            >
              <Delete className={classes.delete} />
            </IconButton>
          </CardActions>
        ) : (
          ""
        )}

        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={
              product.image
                ? `http://localhost:8080/files/images/${product.image[0]}`
                : ""
            }
          ></CardMedia>
          <CardContent className={classes.content}>
            <Typography gutterBottom variant="h5">
              {product.name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {product.description}
            </Typography>
            <Typography variant="h6" component="p">
              R.S {product.price}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};
