import {
  Button,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import { Search } from "@material-ui/icons";
import { useState } from "react";
import { handleError } from "../../utility/handleError";
import { POST } from "../../utility/httpClient";
import { ProductCard } from "../ProductCard/ProductCard.component";

const useStyles = makeStyles((theme) => ({
  formFilled: {
    margin: theme.spacing(2, 0),
  },
  notchedOutline: {
    borderWidth: "1px",
    borderColor: "hotPink !important",
  },
  searchButton: {
    display: "flex",
    justifyContent: "start",
    alignItems: "center",
  },
}));

export const SearchProduct = (props) => {
  const classes = useStyles();
  const [search, setSearch] = useState("");
  const [searchProductRecieved, setSearchProductRecieved] = useState([]);

  const searchedItems = (searchProducts, search) => {
    if (search !== "") {
      const newSearchProduct = searchProducts.filter((products) => {
        return Object.values(products).join("").toLowerCase().includes(search);
      });

      setSearchProductRecieved(newSearchProduct);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!search) return;
    POST("/product/search")
      .then((response) => {
        searchedItems(response.data, search);
      })
      .catch((err) => handleError(err));
  };
  return (
    <>
      <Typography variant="h4" color="secondary">
        Search Product
      </Typography>

      <form onSubmit={handleSubmit}>
        <Grid container>
          <Grid item xs={11}>
            <TextField
              label="search"
              variant="outlined"
              color="secondary"
              onChange={(e) => setSearch(e.target.value)}
              fullWidth
              InputProps={{
                classes: {
                  notchedOutline: classes.notchedOutline,
                },
                style: { color: "hotPink" },
              }}
              InputLabelProps={{
                style: { color: "#ab47bc" },
              }}
              className={classes.formFilled}
            ></TextField>
          </Grid>

          <Grid item xs={1} className={classes.searchButton}>
            <Button color="secondary" type="submit">
              <Search style={{ fontSize: "35px" }} />
            </Button>
          </Grid>
        </Grid>
      </form>

      <Grid container spacing={3}>
        {searchProductRecieved &&
          searchProductRecieved.map((product, index) => (
            <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
              <ProductCard product={product} key={index} isProtected={false} />
            </Grid>
          ))}
      </Grid>
    </>
  );
};
