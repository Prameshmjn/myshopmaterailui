import { Grid, makeStyles } from "@material-ui/core";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { fetchProduct_ac, removeProduct_ac } from "../../Action/Product.ac";
import { ProductCard } from "../ProductCard/ProductCard.component";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const ViewProducComponent = (props) => {
  const classes = useStyles();
  const { products, fetchProducts } = props;
  const [isLoading, setisLoading] = useState(false);
  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);
  const deleteProduct = (productId) => {
    setisLoading(true);
    props.removeProducts(productId, products);
    setisLoading(false);
  };
  return (
    <div className={classes.root}>
      {isLoading ? (
        <h1>Loader Here</h1>
      ) : (
        <Grid container spacing={3}>
          {products.length > 0 &&
            products.map((product, index) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
                <ProductCard
                  product={product}
                  deleteProduct={deleteProduct}
                  isProtected={true}
                />
              </Grid>
            ))}
        </Grid>
      )}
    </div>
  );
};
const mapPropsToState = (rootStore) => ({
  products: rootStore.product.products,
});
const mapDispatchToProps = (dispatch) => ({
  fetchProducts: () => dispatch(fetchProduct_ac()),
  removeProducts: (productId, products) =>
    dispatch(removeProduct_ac(productId, products)),
});
export const ViewProduct = connect(
  mapPropsToState,
  mapDispatchToProps
)(ViewProducComponent);
