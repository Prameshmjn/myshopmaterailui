import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Typography,
} from "@material-ui/core";
import {
  AddCircleOutlined,
  Message,
  NotificationImportant,
  SearchOutlined,
  SubjectOutlined,
} from "@material-ui/icons";
import React from "react";
import { useHistory, useLocation } from "react-router";

const drawerWidth = "240px";

const useStyles = makeStyles((theme) => {
  return {
    drawer: {
      width: drawerWidth,

      [theme.breakpoints.down("sm")]: {
        display: "none",
        width: "100vw",
      },
    },
    drawerPaper: {
      width: drawerWidth,
      [theme.breakpoints.down("sm")]: {
        width: "100vw",
      },
    },
    active: {
      background: "#f4f4f4",
    },
    title: {
      padding: theme.spacing(2),
      marginBottom: theme.spacing(4),
      textAlign: "center",
    },
  };
});

export const Sidebars = () => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const menuItems = [
    {
      text: "Add Product",
      icon: <SubjectOutlined color="secondary" />,
      path: "/addproduct",
    },
    {
      text: "View Product",
      icon: <AddCircleOutlined color="secondary" />,
      path: "/viewproduct",
    },
    {
      text: "Search Product",
      icon: <SearchOutlined color="secondary" />,
      path: "/searchproduct",
    },
    {
      text: "Notifications",
      icon: <NotificationImportant color="secondary" />,
      path: "/notification",
    },
    {
      text: "Messages",
      icon: <Message color="secondary" />,
      path: "/messages",
    },
  ];
  const showSideBar = () => {
    document
      .querySelector(".makeStyles-drawer-12")
      .classList.remove("sidedrawer1");
  };
  let content = localStorage.getItem("token") ? (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      anchor="left"
      classes={{ paper: classes.drawerPaper }}
    >
      <div>
        <Typography variant="h5" className={classes.title}>
          Pramesh
        </Typography>
      </div>

      <List className={classes.listMenbers}>
        {menuItems.map((item) => (
          <ListItem
            button
            key={item.text}
            onClick={() => {
              showSideBar();
              history.push(item.path);
            }}
            className={location.pathname === item.path ? classes.active : null}
          >
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText>{item.text}</ListItemText>
          </ListItem>
        ))}
      </List>
    </Drawer>
  ) : (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      anchor="left"
      classes={{ paper: classes.drawerPaper }}
    >
      <div>
        <Typography variant="h5" className={classes.title}>
          Pramesh
        </Typography>
      </div>

      <List className={classes.listMenbers}>
        {menuItems.map((item) => (
          <ListItem
            button
            key={item.text}
            onClick={() => {
              showSideBar();
              history.push(item.path);
            }}
            className={location.pathname === item.path ? classes.active : null}
          >
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText>{item.text}</ListItemText>
          </ListItem>
        ))}
      </List>
    </Drawer>
  );

  return content;
};
