import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MoreIcon from "@material-ui/icons/MoreVert";
import {
  ArrowForward,
  ArrowUpward,
  Dashboard,
  Home,
  HomeRounded,
  OfflineBolt,
  Person,
  Settings,
} from "@material-ui/icons";
import { useHistory } from "react-router";
import MenuIcon from "@material-ui/icons/Menu";
import "./Navbar.component.css";

const useStyles = makeStyles((theme) => ({
  appbar: {
    background: "#e040fb",
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 240px)`,
    },
  },

  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  title: {
    padding: theme.spacing(2),
    cursor: "pointer",
    "&:hover": {
      color: "blue",
    },
  },
  menuButton: {
    flexGrow: 1,
    paddingLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  menuIcon: {
    fontSize: "25px",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  userName: {
    color: "#e1bee7",
    paddingRight: theme.spacing(1),
  },
  [theme.breakpoints.down("md")]: {
    title: {
      fontSize: "18px",
    },
  },
  showMenu: {
    display: "none",
    flexGrow: 1,
    [theme.breakpoints.down("sm")]: {
      display: "block",
    },
  },
}));

export default function Navbar(props) {
  const classes = useStyles();
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("user"));

  const userName = user ? user.username : "";
  const handleLogout = () => {
    localStorage.clear();
    history.push("/login");
  };

  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };
  const mobileMenuId = "primary-search-account-menu-mobile";

  let mobileView = props.isLoggedOn ? (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      id={mobileMenuId}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton color="secondary">
          <Home />
        </IconButton>
        <p>Home</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="secondary">
          <Person />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="secondary">
          <Dashboard />
        </IconButton>
        <p>Dashboard</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="secondary">
          <Settings />
        </IconButton>
        <p>Setting</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="secondary">
          <OfflineBolt />
        </IconButton>
        <p>Logout</p>
      </MenuItem>
    </Menu>
  ) : (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton color="secondary">
          <ArrowForward />
        </IconButton>
        <p>SignIn</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="secondary">
          <ArrowUpward />
        </IconButton>
        <p>SignUp</p>
      </MenuItem>
    </Menu>
  );

  const renderMobileMenu = mobileView;
  let content = props.isLoggedOn ? (
    <div className={classes.sectionDesktop}>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/home")}
      >
        Home
      </Typography>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/profile")}
      >
        Profile
      </Typography>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/dashboard")}
      >
        Dashboard
      </Typography>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/setting")}
      >
        Setting
      </Typography>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => handleLogout()}
      >
        <small className={classes.userName}>{userName} </small>LOGOUT
      </Typography>
    </div>
  ) : (
    <div className={classes.sectionDesktop}>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/")}
      >
        SignIn
      </Typography>
      <Typography
        variant="h6"
        className={classes.title}
        onClick={() => history.push("/register")}
      >
        SignUp
      </Typography>
    </div>
  );

  const showSideBar = () => {
    document
      .querySelector(".makeStyles-drawer-12")
      .classList.add("sidedrawer1");
  };
  return (
    <div className={classes.grow}>
      <AppBar className={classes.appbar}>
        <Toolbar>
          <Typography className={classes.menuButton}>
            <IconButton edge="start" color="inherit" aria-label="menu">
              <HomeRounded className={classes.menuIcon} />
            </IconButton>
          </Typography>
          <Typography className={classes.showMenu}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={() => showSideBar()}
            >
              <MenuIcon className={classes.menuIcon} />
            </IconButton>
          </Typography>

          <div className={classes.grow} />
          {content}
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}
