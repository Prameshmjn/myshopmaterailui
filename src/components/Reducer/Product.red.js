import { SET_FETCH_PRODUCTS } from "../Action/type";

const defaultState = {
  products: [],
  isLoading: false,
};

export const ProductReducer = (state = { ...defaultState }, action) => {
  console.log("at product reducer");
  console.log("at product reducer action", action);
  switch (action.type) {
    case SET_FETCH_PRODUCTS: {
      return {
        ...state,
        products: action.payload,
      };
    }

    default:
      return { ...state };
  }
};
