import { combineReducers } from "redux";
import { ProductReducer } from "./Product.red";
export const rootReducer = combineReducers({
  product: ProductReducer,
});
