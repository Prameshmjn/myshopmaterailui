import { handleError } from "../utility/handleError";
import { DELETE, GET } from "../utility/httpClient";
import { showInfo } from "../utility/showMessage";
import { SET_FETCH_PRODUCTS, SET_ISLOADING } from "./type";

export const fetchProduct_ac = () => (dispatch) => {
  console.log("at action product");
  console.log("product action data");
  dispatch({
    type: SET_ISLOADING,
    payload: true,
  });
  GET("/product", true)
    .then((response) => {
      dispatch({
        type: SET_FETCH_PRODUCTS,
        payload: response.data,
      });
    })
    .catch((err) => handleError(err))
    .finally(() => {
      dispatch({
        type: SET_ISLOADING,
        payload: false,
      });
    });
};

export const removeProduct_ac = (productId, products) => (dispatch) => {
  console.log("at action removeProduct");
  console.log("remove product data", productId, products);
  DELETE(`/product/${productId}`, true)
    .then((response) => {
      showInfo("product deleted successfully");
      let newProducts = products.filter((product) => product._id !== productId);
      dispatch({
        type: SET_FETCH_PRODUCTS,
        payload: newProducts,
      });
    })
    .catch((err) => handleError(err));
};
