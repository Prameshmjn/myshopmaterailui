import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import { purple } from "@material-ui/core/colors";
import { AppRouting } from "./AppRouting";
import { Provider } from "react-redux";
import { store } from "./store";
import { ToastContainer } from "react-toastify";
const theme = createMuiTheme({
  palette: {
    secondary: purple,
  },
  typography: {
    fontFamily: "Quicksand",
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 600,
    fontWeightBold: 700,
  },
});

export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <AppRouting />
        <ToastContainer />
      </Provider>
    </ThemeProvider>
  );
};
