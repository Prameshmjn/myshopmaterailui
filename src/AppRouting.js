import { Redirect, Route, Switch } from "react-router";
import { BrowserRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import Navbar from "./components/Common/Navbar/Navbar.component";
import { Sidebars } from "./components/Common/Sidebar/Sidebar.component";
import { Login } from "./components/Auth/Login/Login.component";
import { Dashboard } from "./components/Navlinks/Dashboard/Dashboard.component";
import { Home } from "./components/Navlinks/Home/Home.component";
import { Profile } from "./components/Navlinks/Profile/Profile.component";
import { Setting } from "./components/Navlinks/Setting/Setting.component";
import { AddProduct } from "./components/Products/AddProduct/AddProduct.component";
import { ViewProduct } from "./components/Products/ViewProduct/ViewProduct.component";
import { EditProduct } from "./components/Products/EditProduct/EditProduct.component";
import { SearchProduct } from "./components/Products/SearchProduct/SearchProduct.component";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => {
  return {
    root: {
      display: "flex",
      margin: theme.spacing(1),
    },
    pages: {
      width: `calc(100% - ${drawerWidth}px)`,
      [theme.breakpoints.down("md")]: {
        width: "100%",
      },
      margin: theme.spacing(3),
    },
    tooltip: theme.mixins.toolbar,
  };
});
const ProtectedRouting = ({ component: Component, ...rest }) => {
  const classes = useStyles();
  return (
    <Route
      {...rest}
      render={(routeProps) => (
        <div className={classes.root}>
          <Navbar isLoggedOn={localStorage.getItem("token") ? true : false} />
          <Sidebars isLoggedOn={localStorage.getItem("token") ? true : false} />
          <div className={classes.pages}>
            <div className={classes.tooltip} />
            {localStorage.getItem("token") ? (
              <Component {...routeProps} />
            ) : (
              <Redirect to="/"></Redirect>
            )}
          </div>
        </div>
      )}
    ></Route>
  );
};

const PublicRouting = ({ component: Component, ...rest }) => {
  const classes = useStyles();
  return (
    <Route
      {...rest}
      render={(routeProps) => (
        <div className={classes.root}>
          <Navbar isLoggedOn={localStorage.getItem("token") ? true : false} />
          <Sidebars isLoggedOn={localStorage.getItem("token") ? true : false} />
          <div className={classes.pages}>
            <div className={classes.tooltip} />
            <Component {...routeProps} />
          </div>
        </div>
      )}
    ></Route>
  );
};

export const AppRouting = () => {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <PublicRouting path="/" exact component={Login}></PublicRouting>
          <PublicRouting path="/login" component={Login}></PublicRouting>
          <ProtectedRouting
            path="/dashboard"
            component={Dashboard}
          ></ProtectedRouting>
          <ProtectedRouting path="/home" component={Home}></ProtectedRouting>
          <ProtectedRouting
            path="/profile"
            component={Profile}
          ></ProtectedRouting>
          <ProtectedRouting
            path="/setting"
            component={Setting}
          ></ProtectedRouting>
          <ProtectedRouting
            path="/addproduct"
            component={AddProduct}
          ></ProtectedRouting>
          <ProtectedRouting
            path="/viewproduct"
            component={ViewProduct}
          ></ProtectedRouting>
          <ProtectedRouting
            path="/editproduct/:productId"
            component={EditProduct}
          ></ProtectedRouting>
          <PublicRouting
            path="/searchproduct"
            component={SearchProduct}
          ></PublicRouting>
        </Switch>
      </BrowserRouter>
    </>
  );
};
